#!/bin/sh -x

src_path=~/asuswrt

ln -sf $src_path/tools/brcm /opt/brcm
ln -sf $src_path/release/src-rt/toolchains/hndtools-arm-linux-2.6.36-uclibc-4.5.3 /opt/brcm-arm
export PATH=$PATH:/opt/brcm/hndtools-mipsel-linux/bin:/opt/brcm/hndtools-mipsel-uclibc/bin:/opt/brcm-arm/bin
 
cd $src_path/release/src-rt
LOG=$src_path/log$(date '+%Y%m%d%H%M%S').txt
 
time -f "%E real, %U user, %S sys" make rt-ac1200g+ | tee $LOG 

